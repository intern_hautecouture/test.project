﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;

public enum BackKeyAction
{
    QuitGame,
    SceneChange,
    InvokeEvent,
}

public class BackButtonUtil : MonoBehaviour {

    [SerializeField]
    BackKeyAction backKeyAction;

    // SceneChange
    [SerializeField]
    string nextSceneName;

    // InvokeEvent
    [SerializeField]
    UnityEvent[] onClickBack;

    int currentEvent = 0;

    // ----- Instance -----
    static BackButtonUtil instance;
    public static BackButtonUtil Instance
    {
        get {
            if (instance == null)
            {
                instance = FindObjectOfType(typeof(BackButtonUtil)) as BackButtonUtil;
                if (instance == null)
                {
                    Debug.Log("BackButtonUtil is nothing");
                }
            }
            return instance;
        }
    }

    // ----- Method -----
    void Update()
    {
        GetBackKey();
    }

    void GetBackKey()
    {
        //if (Application.platform != RuntimePlatform.Android) return;
        if (!Input.GetKeyDown(KeyCode.Escape)) return;

        switch (backKeyAction)
        {
            case BackKeyAction.QuitGame:
                Application.Quit();
#if UNITY_EDITOR
                Debug.Break();
#endif
                break;
            case BackKeyAction.SceneChange:
                Application.LoadLevel(nextSceneName);
                break;
            case BackKeyAction.InvokeEvent:
                onClickBack[currentEvent].Invoke();
                if (++currentEvent >= onClickBack.Length) currentEvent = 0;
                break;
        }
    }

    // ----- ChangeAction -----
    public void ChangeAction(BackKeyAction action) {
        backKeyAction = action;
    }

    // ----- AddListener -----
    public void AddListener(int index, UnityAction action)
    {
        onClickBack[index].AddListener(action);
    }

    public void AddListener(int index, UnityAction action1, UnityAction action2)
    {
        onClickBack[index].AddListener(action1);
        onClickBack[index].AddListener(action2);
    }

    public void AddListener(int index, UnityAction action1, UnityAction action2, UnityAction action3)
    {
        onClickBack[index].AddListener(action1);
        onClickBack[index].AddListener(action2);
        onClickBack[index].AddListener(action3);
    }

    // ----- Remove and Add Listener -----
    public void ClearAndAddListener(int index, UnityAction action)
    {
        RemoveIndexListenerAll(index);
        AddListener(index, action);
    }

    public void ClearAndAddListener(int index, UnityAction action1, UnityAction action2)
    {
        RemoveIndexListenerAll(index);
        AddListener(index, action1, action2);
    }

    public void ClearAndAddListener(int index, UnityAction action1, UnityAction action2, UnityAction action3)
    {
        RemoveIndexListenerAll(index);
        AddListener(index, action1, action2, action3);
    }

    // ----- RemoveListener -----
    public void RemoveListener(int index, UnityAction action)
    {
        onClickBack[index].RemoveListener(action);
    }

    public void RemoveListener(int index, UnityAction action1, UnityAction action2)
    {
        onClickBack[index].RemoveListener(action1);
        onClickBack[index].RemoveListener(action2);
    }

    public void RemoveListener(int index, UnityAction action1, UnityAction action2, UnityAction action3)
    {
        onClickBack[index].RemoveListener(action1);
        onClickBack[index].RemoveListener(action2);
        onClickBack[index].RemoveListener(action3);
    }

    public void RemoveIndexListenerAll(int index)
    {
        onClickBack[index].RemoveAllListeners();
    }

    public void RemoveListenerAll()
    {
        for (int i = 0; i < onClickBack.Length - 1; i++)
        {
            RemoveIndexListenerAll(i);
        }
    }

    void Awake()
    {
        AdmobManager.ShowInterstitial();
    }
}
