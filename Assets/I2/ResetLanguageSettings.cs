﻿using UnityEngine;
using System.Collections;
using I2.Loc;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class ResetLanguageSettings : MonoBehaviour {

#if UNITY_EDITOR
    [MenuItem("I2 Extension/Reset Language")]
#endif
    static void ResetFromEditor()
    {
        ResetLanguage();
    }

    public static void ResetLanguage()
    {
        PlayerPrefs.DeleteKey("I2 Language");
        LocalizationManager.InitializeManually();
    }
}

/*
 * 必要に応じて下記関数を LocalizationManager に追記.
        public static void InitializeManually()
        {
            mCurrentLanguage = "";
            InitializeIfNeeded();
        }
 */
