﻿using UnityEngine;
using System.Collections;

public class MenuScene : MonoBehaviour {

	// Boolean to check Music and Sound On/Off states
	public static bool soundOn = true;
	public static bool musicOn = true;

	// Menu scene music clip
	public AudioClip musicClip;
	public static AudioSource musicSource;

	// Variable to store current Game Mode
	public static int modeNum = 1;

	// Ninja Sprites
	private GameObject ninja1, ninja2;

	// Boolean for paused and game over states
	public static bool paused;
	public static bool isGameOver;

	// Variable to store current score in each game mode
	public static int scoreNum;

	// Use this for initialization
	void Start () {

		// Get ninjas sprites from scene
		ninja1 = GameObject.Find("n13");
		ninja2 = GameObject.Find("n14");

		// Loop ninjas show/hide
		StartCoroutine(loopNinjas());

		// Initialize music
		musicSource = gameObject.AddComponent<AudioSource>();
		musicSource.playOnAwake = false;
		musicSource.rolloffMode = AudioRolloffMode.Linear;
		musicSource.loop = true;
		musicSource.clip = musicClip;

		if(musicOn){
			musicSource.Play(); // Play music if sound is on
		}

		Time.timeScale = 1; // Use normal game speed. If previously paused, it would be 0

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)){
			Application.Quit(); // Quit game when back key is pressed
		}
	}

	// Method to loop ninjas
	IEnumerator loopNinjas(){
		int a = 1;
		while(true){
			yield return new WaitForSeconds(Random.Range(1, 4));
			if(a == 1){
				a = 0;
				Ninja ninjaScript = ninja1.GetComponent<Ninja>();
				ninjaScript.pop();
			}else{
				a = 1;
				Ninja ninjaScript = ninja2.GetComponent<Ninja>();
				ninjaScript.pop();
			}
		}
	}

}






