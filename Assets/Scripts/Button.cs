﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Button : MonoBehaviour {


	/// <summary>
	/// Button Script
	/// Add this script to the buttons and it will determine
	/// the button being clicked and execute the required code.
	/// </summary>

    [SerializeField]
    Canvas pauseCanvas;

	// Color for button clicked state
	public Color colorPushed  = new Color(0.8f,0.8f,0.8f);   
	private Color originalColor;

	// Sound and Music buttons variable
	private GameObject btnSoundOn;
	private GameObject btnSoundOff;

    [SerializeField]
    private Image btnMusicCheak;

    [SerializeField]
    private Image btnSoundCheck;

	//private GameObject btnHome;
	//private GameObject btnResume;


	// Use this for initialization
    void Start()
    {

    }

    public void GamePlaybtn()
    {
        Application.LoadLevel(Levels.Modes);
    }

    public void Quitbtn()
    {
        Application.Quit();
    }

    public void Normalbtn()
    {
        Application.LoadLevel(Levels.NormalMode);
    }

    public void Hostagebtn()
    {
        Application.LoadLevel(Levels.HostageMode);
    }

    public void Timerbtn()
    {
        Application.LoadLevel(Levels.TimerMode);
    }

    public void Musicbtn()
    {
        if (MenuScene.musicOn)
        {

            btnMusicCheak.enabled = false;
            MenuScene.musicOn = false;
            MenuScene.musicSource.Stop();
        }

        else
        {
            btnMusicCheak.enabled = true;
            MenuScene.musicOn = true;
            MenuScene.musicSource.Play();
        }
      
    }

    public void Soundbtn()
    {
        if (MenuScene.soundOn)
        {

            btnSoundCheck.enabled = false;
            MenuScene.soundOn = false;
        }

        else
        {
            btnSoundCheck.enabled = true;
            MenuScene.soundOn = true;
        }

    }

    public void NextScene()
    {
        if(gameObject.name == "btn-modes-normal")
		{
			Application.LoadLevel(Levels.NormalMode);
		}else if(gameObject.name == "btn-modes-hostage")
		{
			Application.LoadLevel(Levels.HostageMode);
		}else if(gameObject.name == "btn-modes-timer")
		{
			Application.LoadLevel(Levels.TimerMode);
		}

    }

    public void Buttons()
    {
        if (!MenuScene.paused)
        {
            Time.timeScale = 0; // pause
            MenuScene.paused = true;
            GameObject.Find("TextScore").GetComponent<Text>().enabled = false;
            pauseCanvas.gameObject.SetActive(true);

            if (MenuScene.modeNum == 1)
            {
                GameScene.musicSource.Stop();
                GameObject.Find("TextMisses").GetComponent<Text>().enabled = false;
            }
            else if (MenuScene.modeNum == 2)
            {
                HostageScene.musicSource.Stop();
                //GameObject.Find("TextHostages").GetComponent<Text>().enabled = false;
            }
            else if (MenuScene.modeNum == 4)
            {
                TimerGameScene.musicSource.Stop();
                GameObject.Find("TextTime").GetComponent<Text>().enabled = false;
            }
        }
    }


    public void Paused()
    {
        if (MenuScene.paused && gameObject.name == "btn-resume")
        {
            Time.timeScale = 1; // resume
            MenuScene.paused = false;
            GameObject.Find("TextScore").GetComponent<Text>().enabled = true;
            

            if (MenuScene.modeNum == 1)
            {
                if (MenuScene.musicOn) GameScene.musicSource.Play();
                GameObject.Find("TextMisses").GetComponent<Text>().enabled = true;
            }
            else if (MenuScene.modeNum == 2)
            {
                if (MenuScene.musicOn) HostageScene.musicSource.Play();
                //GameObject.Find("TextHostages").GetComponent<Text>().enabled = true;
            }
            else if (MenuScene.modeNum == 4)
            {
                if (MenuScene.musicOn) TimerGameScene.musicSource.Play();
                GameObject.Find("TextTime").GetComponent<Text>().enabled = true;
            }

            pauseCanvas.gameObject.SetActive(false);

        }
        else if ((MenuScene.paused && gameObject.name == "btn-home") || (Application.loadedLevel == Levels.GameOver))
        {
            Application.LoadLevel(Levels.Menu);
        }
    }

    public void Replay()
    {
        if (gameObject.name == "btn-play" || gameObject.name == "btn-replay")
        {
            Application.LoadLevel(Levels.Modes);
        }
    }

    //public void OnMouseUpAsButton()
    //{
    //    if (gameObject.name == "btn-play" || gameObject.name == "btn-replay")
    //    {
    //        Application.LoadLevel(Levels.Modes);
    //    }
    //    else if (gameObject.name == "btn-quit")
    //    {
    //        Application.Quit();
    //    }
    //    else if (gameObject.name == "btn-modes-normal")
    //    {
    //        Application.LoadLevel(Levels.NormalMode);
    //    }
    //    else if (gameObject.name == "btn-modes-hostage")
    //    {
    //        Application.LoadLevel(Levels.HostageMode);
    //    }
    //    else if (gameObject.name == "btn-modes-timer")
    //    {
    //        Application.LoadLevel(Levels.TimerMode);
    //    }
    //    else if (gameObject.name == "btn-sOn")
    //    {
    //        if (MenuScene.soundOn)
    //        {
    //            btnSoundOn.renderer.enabled = false;
    //            btnSoundOff.renderer.enabled = true;
    //            MenuScene.soundOn = false;
    //        }
    //        else
    //        {
    //            btnSoundOff.renderer.enabled = false;
    //            btnSoundOn.renderer.enabled = true;
    //            MenuScene.soundOn = true;
    //        }
    //    }
    //    else if (gameObject.name == "btn-mOn")
    //    {

    //    }
        
    //    else if (Time.timeScale == 0 && gameObject.name == "btn-info-out")
    //    {
    //        Time.timeScale = 1;
    //        //GameObject.Find("bg-paused").renderer.enabled = false;
    //        //GameObject.Find("TextInfo").guiText.enabled = false;
    //        //GameObject.Find("btn-info-out").renderer.enabled = false;

    //        if (MenuScene.musicOn)
    //        {
    //            GameScene.musicSource.Play(); // resume music
    //        }
    //        PlayerPrefs.SetInt("FirstPlay", 0);
    //    }
    //}

}
