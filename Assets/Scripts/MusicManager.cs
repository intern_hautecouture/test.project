﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

    public static MusicManager Instance
    {
        get;
        private set;
    }

    public static AudioSource musicSource;

    [SerializeField]
    private AudioClip menuMusic;


    void Start()
    {
        musicSource = GetComponent<AudioSource>();
        musicSource.clip = menuMusic;
    }

    public void MusicPlay()
    {

        
    }
    
}
