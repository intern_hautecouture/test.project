﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HostageScene : MonoBehaviour {
	
	/// <summary>
	/// 
	/// Refer GameScene.cs file for detail commments
	/// The Source Code of both the scripts are same with minor changes
	/// 
	/// </summary>


	private GameObject[] ninjas1;
	private GameObject[] ninjas2;
	private GameObject[] ninjas3;
	private GameObject[] ninjas4;
	
	private bool[] isHoleTaken;
	
	private GameObject textTime;
	private static GameObject textScore;
	private static GameObject textHostages;
	
	private static GameObject hammer;
	private static GameObject blam;
    private static GameObject neil;
	
	private int timeNum;
	public static int hostagesNum;
	
	
	//	public Camera camera;
	
	public AudioClip hitClip;
	private static AudioSource hitSource;

    public AudioClip missClip;
    private static AudioSource missSource;
	
	public AudioClip musicClip;
	public static AudioSource musicSource;
	
	private bool overSceneLoaded;
	// Use this for initialization
	void Start () {
		
		
		// Get screen borders
		//		float dist = (transform.position - Camera.main.transform.position).z;
		//		leftX = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
		//		rightX = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
		//		topY = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, -dist)).y;
		//		bottomY = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).y;
		
		//GameObject.Find("title-paused").renderer.enabled = false;
		//GameObject.Find("btn-resume").renderer.enabled = false;
		//GameObject.Find("btn-home").renderer.enabled = false;
		//GameObject.Find("bg-paused").renderer.enabled = false;
		
		//		camera = GameObject.Find("Main Camera");
		hitSource = gameObject.AddComponent<AudioSource>();
		hitSource.playOnAwake = false;
		hitSource.rolloffMode = AudioRolloffMode.Linear;
		hitSource.clip = hitClip;

        missSource = gameObject.AddComponent<AudioSource>();
        missSource.playOnAwake = false;
        missSource.rolloffMode = AudioRolloffMode.Linear;
        missSource.clip = missClip;

		musicSource = gameObject.AddComponent<AudioSource>();
		musicSource.playOnAwake = false;
		musicSource.rolloffMode = AudioRolloffMode.Linear;
		musicSource.loop = true;
		musicSource.clip = musicClip;
		
		if(MenuScene.musicOn){
			musicSource.Play();
		}
		
		textTime = GameObject.Find("TextTime");
		textScore = GameObject.Find("TextScore");
		textHostages = GameObject.Find("TextHostages");
		
		hammer = GameObject.Find("hammer");
		hammer.GetComponent<SpriteRenderer>().enabled = false;
		
		blam = GameObject.Find("blam");
		blam.GetComponent<SpriteRenderer>().enabled = false;

        neil = GameObject.Find("neil");
        neil.GetComponent<SpriteRenderer>().enabled = false;
		
		timeNum = 90;
		MenuScene.scoreNum = 0;
		hostagesNum = 0;
		MenuScene.paused = false;
		MenuScene.isGameOver = false;
		overSceneLoaded = false;
		MenuScene.modeNum = 2;
		
		isHoleTaken = new bool[7];
		isHoleTaken[0] = false;
		isHoleTaken[1] = false;
		isHoleTaken[2] = false;
		isHoleTaken[3] = false;
		isHoleTaken[4] = false;
		isHoleTaken[5] = false;
		isHoleTaken[6] = false;
		
		ninjas1 = new GameObject[7];
		ninjas1[0] = GameObject.Find("n10");
		ninjas1[1] = GameObject.Find("n11");
		ninjas1[2] = GameObject.Find("n12");
		ninjas1[3] = GameObject.Find("n13");
		ninjas1[4] = GameObject.Find("n14");
		ninjas1[5] = GameObject.Find("n15");
		ninjas1[6] = GameObject.Find("n16");
		
		ninjas2 = new GameObject[7];
		ninjas2[0] = GameObject.Find("n20");
		ninjas2[1] = GameObject.Find("n21");
		ninjas2[2] = GameObject.Find("n22");
		ninjas2[3] = GameObject.Find("n23");
		ninjas2[4] = GameObject.Find("n24");
		ninjas2[5] = GameObject.Find("n25");
		ninjas2[6] = GameObject.Find("n26");
		
		ninjas3 = new GameObject[7];
		ninjas3[0] = GameObject.Find("n30");
		ninjas3[1] = GameObject.Find("n31");
		ninjas3[2] = GameObject.Find("n32");
		ninjas3[3] = GameObject.Find("n33");
		ninjas3[4] = GameObject.Find("n34");
		ninjas3[5] = GameObject.Find("n35");
		ninjas3[6] = GameObject.Find("n36");
		
		ninjas4 = new GameObject[7];
		ninjas4[0] = GameObject.Find("n40");
		ninjas4[1] = GameObject.Find("n41");
		ninjas4[2] = GameObject.Find("n42");
		ninjas4[3] = GameObject.Find("n43");
		ninjas4[4] = GameObject.Find("n44");
		ninjas4[5] = GameObject.Find("n45");
		ninjas4[6] = GameObject.Find("n46");
		
		//		Ninja ninja14Script = ninjas1[4].GetComponent<Ninja>();
		//		ninja14Script.pop();
		
		StartCoroutine(popNinjas());
		//		StartCoroutine(countDownTimer());
		
		//		if(PlayerPrefs.GetInt("FirstPlay", 1) == 1){
		//			Time.timeScale = 0;
		//			GameObject.Find("bg-paused").renderer.enabled = true;
		//			GameObject.Find("TextInfo").guiText.enabled = true;
		//			GameObject.Find("btn-info-out").renderer.enabled = true;
		//			if(MenuScene.musicOn){
		//				musicSource.Pause(); // resume music
		//			}
		//		}
		
		//		Tap on the Ninjas to smash them 
		//			before they hide inside the hole. 
		//				Game's over if you miss 200 times.



	}
	
	// Update is called once per frame
	void Update () {
		//		if(Input.GetKeyDown(KeyCode.Escape)){
		//			Application.LoadLevel(0);
		//		}
		
		if (Input.GetButtonDown ("Fire1")) {
			Vector3 mousePos = Input.mousePosition;
			mousePos.z = 10;
			Vector3 wp = Camera.main.ScreenToWorldPoint(mousePos);
			Vector2 touchPos = new Vector2(wp.x + 0.3f, wp.y);
			//			Debug.Log("X= "+ wp.x + "   Y= "+ wp.y);
			showHammer(touchPos);
		}
		
		if (Input.touchCount == 1)
		{
			Touch touch = Input.touches[0];
			if(touch.phase == TouchPhase.Began){
				Vector3 mousePos = touch.position; // Input.GetTouch(0).position;
				mousePos.z = 10;
				Vector3 wp = Camera.main.ScreenToWorldPoint(mousePos);
				Vector2 touchPos = new Vector2(wp.x + 0.3f, wp.y);
				showHammer(touchPos);
			}
			
		}
		/*
		if(Input.GetKeyDown(KeyCode.Escape)){
			if(!MenuScene.paused){
				Time.timeScale = 0; // pause
				MenuScene.paused = true;
				// Make paused screen elements visible when paused
				//GameObject.Find("bg-paused").renderer.enabled = true;
				//GameObject.Find("btn-resume").renderer.enabled = true;
				//GameObject.Find("btn-home").renderer.enabled = true;
				//GameObject.Find("title-paused").renderer.enabled = true;
				GameObject.Find("TextHostages").GetComponent<Text>().enabled = false;
                GameObject.Find("TextScore").GetComponent<Text>().enabled = false;
				musicSource.Stop(); // stop music
			}else{
				Time.timeScale = 1; // resume
				MenuScene.paused = false;
				// Hide paused screen elements when resumed
				//GameObject.Find("bg-paused").renderer.enabled = false;
				//GameObject.Find("btn-resume").renderer.enabled = false;
				//GameObject.Find("btn-home").renderer.enabled = false;
				//GameObject.Find("title-paused").renderer.enabled = false;
                GameObject.Find("TextHostages").GetComponent<Text>().enabled = true;
                GameObject.Find("TextScore").GetComponent<Text>().enabled = true;
				if(MenuScene.musicOn){
					musicSource.Play(); // resume music
				}
			}
		}
		*/
		
		if(MenuScene.isGameOver && !overSceneLoaded){
			overSceneLoaded = true;
			Application.LoadLevel(Levels.GameOver);
		}
	}
	
	IEnumerator popNinjas() {
		while (!MenuScene.isGameOver){
			yield return new WaitForSeconds(Random.Range(0.2f, 0.5f));
			{
				int randomHole;
				while(true){
					int rand = Random.Range(1, 8);
					if(!isHoleTaken[rand-1]){
						randomHole= rand-1;
						break;
					}
				}
				
				int randomNinjaType = Random.Range(1, 5);

				/*
				Ninja ninjaScript;
				if(randomNinjaType == 1){
					ninjaScript = ninjas1[randomHole].GetComponent<Ninja>();
				}else if(randomNinjaType == 2){
					ninjaScript = ninjas2[randomHole].GetComponent<Ninja>();
				}else if(randomNinjaType == 3){
					ninjaScript = ninjas3[randomHole].GetComponent<Ninja>();
				}else{
					ninjaScript = ninjas4[randomHole].GetComponent<Ninja>();
				}
				ninjaScript.pop();
				*/
				
				isHoleTaken[randomHole] = true;
				StartCoroutine(changeHoleState(randomHole));
			}
		}
	}
	
	IEnumerator changeHoleState(int holeNum){
		yield return new WaitForSeconds(1.5f);
		isHoleTaken[holeNum] = false;
	}
	
	IEnumerator countDownTimer(){
		while(timeNum > 0){
			yield return new WaitForSeconds(1);
			timeNum -= 1;
			textTime.guiText.text = timeNum + " secs";
			if(timeNum == 0){
				MenuScene.isGameOver = true;
			}
		}
	}
	
	public static void updateScore(bool positive){
		if(positive){
			MenuScene.scoreNum += 1;
            textScore.GetComponent<Text>().text = MenuScene.scoreNum.ToString();
		}else{
			hostagesNum += 1;
            textHostages.GetComponent<Text>().text = hostagesNum.ToString();
			if(hostagesNum == 5){
				MenuScene.isGameOver = true;
			}
		}
	}
	
//	public static void updateMisses(){
//		missesNum += 1;
//		textMisses.guiText.text = "Misses: "+ missesNum;
//		if(missesNum == 200){
//			MenuScene.isGameOver = true;
//		}
//	}
	
	public void showHammer(Vector2 pos){
		hammer.transform.position = pos;
		hammer.GetComponent<SpriteRenderer>().enabled = true;
		hammer.GetComponent<Animator>().SetTrigger("fire");
	}
	
	public static void showBlam(Vector2 pos){
		blam.transform.position = pos;
		blam.GetComponent<SpriteRenderer>().enabled = true;
		if(MenuScene.soundOn){
			hitSource.Play();
		}
		StaticCoroutine.DoCoroutine(hideBlam());
        if(neil.GetComponent<SpriteRenderer>().enabled == true)
        {
            StaticCoroutine.DoCoroutine(hideNeil());
        }
	}
	
	private static IEnumerator hideBlam(){
		yield return new WaitForSeconds(0.6f);
		blam.GetComponent<SpriteRenderer>().enabled = false;
	}

    public static void showNeil()
    {
        //neil.transform.position = pos;
        neil.GetComponent<SpriteRenderer>().enabled = true;
        if (MenuScene.soundOn)
        {
            missSource.Play();
        }
        StaticCoroutine.DoCoroutine(hideNeil());
    }

    private static IEnumerator hideNeil()
    {
        yield return new WaitForSeconds(0.6f);
        neil.GetComponent<SpriteRenderer>().enabled = false;
    }

	//	void OnMouseDown() {
	//
	//	}
	
}




