﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOverScene : MonoBehaviour {

	/// <summary>
	/// Game Over scene script.
	/// Shows current and best scores. Play and Menu buttons
	/// </summary>


	// Variables for GUI Text objects for score and best score;
	private GameObject textScore;
	private GameObject textBest;

	// Variables for score and best score values
	private int scoreNum;
	private int bestNum;

	// Use this for initialization
	void Start () {

		// Get gui texts from game scene
		textScore = GameObject.Find("TextScore");
		textBest = GameObject.Find("TextBest");

		// Get current score
		scoreNum = MenuScene.scoreNum;

		// Get Stored best number value according tot he current game mode
		string fileText = "";
		if(MenuScene.modeNum == 1){
			fileText = "best1";
		}else if(MenuScene.modeNum == 2){
			fileText = "best2";
		}else if(MenuScene.modeNum == 4){
			fileText = "best4";
		}
		bestNum = PlayerPrefs.GetInt(fileText, 0);

		// If current score is greater than previously stored value, save best score
		if(scoreNum > bestNum){
			bestNum = scoreNum;
			PlayerPrefs.SetInt(fileText, scoreNum);
		}

		// Now show the current and best score values in screen
        textScore.GetComponent<Text>().text = scoreNum.ToString();
        textBest.GetComponent<Text>().text = bestNum.ToString();

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
