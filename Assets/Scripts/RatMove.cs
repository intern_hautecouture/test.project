﻿using UnityEngine;
using System.Collections;

public class RatMove : MonoBehaviour 
{
	//Private Loacal Area
	public float SelectMoveSpeed;

	private float RatX;
	private float RatY;
	private float Xmove_Speed;
	
	private int StopingMarzin;
	private bool stopFlg;

	private bool Turn;

	private bool MoveAction;
	private bool StopAction;

	protected Animation animation;
	protected Animator animator;

	public int TimeMarzin;
	public int MAX_Marzin;
	
	// Use this for initialization
	void Start () 
	{
		animator = GetComponent<Animator> ();
	}
	
	// Update is called once per frame
	void Update () 
	{
		this.TimeMarzin++;

		//Scale Update 
		Vector3 Scale = transform.localScale;

		//if (animator) 
		//{
			//Left Vectoll
			if (Turn == false) 
			{
				this.Xmove_Speed = this.SelectMoveSpeed;
				Scale.x = 1f;
			}

			//Right Vectoll
			if (Turn == true) 
			{
				this.Xmove_Speed = this.SelectMoveSpeed;
				Scale.x = -1f;
			}
				
			this.RatX = this.Xmove_Speed;
			transform.localScale = Scale;

			//Update Tramsform
			transform.Translate (this.RatX, 0, 0, Space.World);

		if (this.TimeMarzin > this.MAX_Marzin && this.Turn == false) 
		{
			this.Turn = true;
			this.SelectMoveSpeed = +0.1f;
			this.TimeMarzin=0;
		}

		if (this.TimeMarzin > this.MAX_Marzin && this.Turn == true) 
		{
			this.Turn = false;
			this.SelectMoveSpeed = -0.1f;
			this.TimeMarzin=0;
		}


			//animator.SetBool ("Stop", this.StopAction);
			//animator.SetBool ("Move", this.MoveAction);
		//}
	}
}
