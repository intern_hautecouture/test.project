﻿using UnityEngine;
using System.Collections;

public class Mouse_Controll : MonoBehaviour 
{
	public float RatX; 
	public float RatY;
	public float X_MoveSpeed;
	
	public int Type;
	public int StopMarzin;

	public bool Turn;

	public Sprite MoveRatSprite;
	public Sprite MoveRatSprite2;
	public Sprite StopRatSprite;

	public GameObject RatObject;

	private float RandomStopPosition;
	private int RandomStopMarzin;
	private int AnimationChange;

	private bool canSmash;

	SpriteRenderer thisRenderer;

	// Use this for initialization
	void Start () 
	{
		thisRenderer = this.GetComponent<SpriteRenderer>();
		this.RandomStopPosition = Random.Range (-8.0f, 5.0f);
		this.RandomStopMarzin = Random.Range (30, 100);

		this.canSmash = false;
		//this.X_MoveSpeed = Random.value;
	}
	
	// Update is called once per frame
	void Update () 
	{
		Vector3 RatScale = transform.localScale;

		if (Input.touchCount == 1)
		{
			Vector3 tPos = Input.GetTouch(0).position;
			tPos.z = 10;
			Vector3 wp = Camera.main.ScreenToWorldPoint(tPos);
			Vector2 touchPos = new Vector2(wp.x, wp.y);

			if (collider2D == Physics2D.OverlapPoint(touchPos))
			{
				if(canSmash)
				{
					if(MenuScene.modeNum == 1)
					{
						GameScene.updateScore();
					}
					else if(MenuScene.modeNum == 2)
					{
						string name = gameObject.name;
						if(name == "mouse_1")
						{
							HostageScene.updateScore(true);
						}
						else
						{
							HostageScene.updateScore(false);
						}
					}
					else if(MenuScene.modeNum == 4)
					{
						TimerGameScene.updateScore();
					}
						
					collider2D.enabled = false;
					canSmash = false;
					if(MenuScene.modeNum == 1)
					{
						GameScene.showBlam(new Vector2(transform.position.x, transform.position.y));
						this.RatObject.transform.position = new Vector3(10.0f + Random.Range(3.0f, 10.0f), Random.Range(-3.0f,3.0f), 0);
					}
					else if(MenuScene.modeNum == 2)
					{
                        if (gameObject.name == "mouse_1") {
                            HostageScene.showBlam(new Vector2(transform.position.x, transform.position.y));
                        }
                        else if (gameObject.name == "Cat_1")
                        {
                            HostageScene.showNeil();
                        }

						this.RatObject.transform.position = new Vector3(10.0f + Random.Range(3.0f, 10.0f), Random.Range(-3.0f,3.0f), 0);
					}
					else if(MenuScene.modeNum == 4)
					{
						TimerGameScene.showBlam(new Vector2(transform.position.x, transform.position.y));
						this.RatObject.transform.position = new Vector3(10.0f + Random.Range(3.0f, 10.0f), Random.Range(-3.0f,3.0f), 0);
					}
				}
			}
		}


		if (this.Turn == false) 
		{
			if(this.RatObject.transform.position.x > this.RandomStopPosition)
			{
				this.X_MoveSpeed = -12;

				this.AnimationChange++;

				if(this.AnimationChange <= 10)
				{
					this.thisRenderer.sprite = this.MoveRatSprite;
				}
				else if(this.AnimationChange <=20 && this.AnimationChange > 10)
				{
					this.thisRenderer.sprite = this.MoveRatSprite2;
				}
				else if(this.AnimationChange > 20)
				{
					this.AnimationChange=0;
				}
				//GameScene.updateScore();
			}
			else if(this.RatObject.transform.position.x < this.RandomStopPosition && this.StopMarzin < this.RandomStopMarzin)
			{
				this.X_MoveSpeed = 0.0f;
				collider2D.enabled = true;
				this.thisRenderer.sprite = this.StopRatSprite;
				this.StopMarzin++;

				this.canSmash = true;

			}
			else if(this.RatObject.transform.position.x > -15 && this.StopMarzin >= RandomStopMarzin)
			{
				this.X_MoveSpeed = -12f;

				this.AnimationChange++;
				
				if(this.AnimationChange <= 10)
				{
					this.thisRenderer.sprite = this.MoveRatSprite;
				}
				else if(this.AnimationChange <=20 && this.AnimationChange > 10)
				{
					this.thisRenderer.sprite = this.MoveRatSprite2;
				}
				else if(this.AnimationChange > 20)
				{
					this.AnimationChange=0;
				}
                //どのタイミングでも叩けるように変更.
				//collider2D.enabled = false;
			}
			else if ((this.RatObject.transform.position.x <= -15))
			{
				this.RatObject.transform.position = new Vector3(10.0f + Random.Range(3.0f, 10.0f), Random.Range(-3.0f,3.0f), 0);
				this.StopMarzin=0;
				this.RandomStopPosition = Random.Range (-8.0f, 5.0f);

				if(MenuScene.modeNum == 1)
				{
					GameScene.updateMisses();
				}
			}

			RatScale.x = 1.0f;
		}
		if (this.Turn == true) 
		{
			this.X_MoveSpeed = 0.1f;
			RatScale.x = -1.0f;
		}

		this.RatX = this.X_MoveSpeed;
        transform.Translate(this.RatX * Time.deltaTime, this.RatY * Time.deltaTime, 0, Space.World);
		transform.localScale = RatScale;
	}

	void OnMouseDown() 
	{
		if(canSmash)
		{
			if(MenuScene.modeNum == 1)
			{
				GameScene.updateScore();
			}
			else if(MenuScene.modeNum == 2)
			{
				string name = gameObject.name;
				if(name == "mouse_1")
				{
					HostageScene.updateScore(true);
				}
				else
				{
					HostageScene.updateScore(false);
				}
			}
			else if(MenuScene.modeNum == 4)
			{
				TimerGameScene.updateScore();
			}
			
			canSmash = false;
			collider2D.enabled = false;
			if(MenuScene.modeNum == 1)
			{
				GameScene.showBlam(new Vector2(transform.position.x, transform.position.y));
				this.RatObject.transform.position = new Vector3(10.0f + Random.Range(3.0f, 10.0f), Random.Range(-3.0f,3.0f), 0);
			}
			else if(MenuScene.modeNum == 2)
			{
                if (gameObject.name == "mouse_1")
                {
                    HostageScene.showBlam(new Vector2(transform.position.x, transform.position.y));
                }
                else if (gameObject.name == "Cat_1")
                {
                    HostageScene.showNeil();
                }
				this.RatObject.transform.position = new Vector3(10.0f + Random.Range(3.0f, 10.0f), Random.Range(-3.0f,3.0f), 0);
			}
			else if(MenuScene.modeNum == 4)
			{
				TimerGameScene.showBlam(new Vector2(transform.position.x, transform.position.y));
				this.RatObject.transform.position = new Vector3(10.0f + Random.Range(3.0f, 10.0f), Random.Range(-3.0f,3.0f), 0);
			}
		}
	}
	
}
