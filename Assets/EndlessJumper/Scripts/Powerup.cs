﻿using UnityEngine;
using System.Collections;

public class Powerup : MonoBehaviour {

	bool jump;
	GameObject player;
	public float[] itemPower;
	public GameManager Game;
	public int itemType;
	public SpriteRenderer mySpriteRenderer;
	public Sprite[] mySprites;
	public GameObject[] myObjects;

	public SoundManager SFXManager;

	public Sprite Stand;
	public Sprite Rocket;

	
	void OnEnable()
	{

		//Assign the type - which item is it 

		//0 = Jetpack Blue
		//1 = Jetpack Orange
		//2 = Spring Boots
		//3 = Shield
	


		
		string typeOfPowerup = this.tag.ToString().Replace("ItemType","");
		
		
		itemType = int.Parse(typeOfPowerup);
		Debug.Log("Item type is " + itemType); 

		mySpriteRenderer.sprite = mySprites[itemType];
	
	}


	void OnTriggerEnter2D(Collider2D col)
	{

		if(col.name.Contains("Player"))
		{
			SFXManager.playSFX(3);
			if(itemType==2)//Item type is 2 = spring boots
			{
				//Add to score
				Game.player.isSpringBoot = true;
				Game.player.switchOnSpringBoots();
				Game.addInactiveItem(this.gameObject);


			}
			else if(itemType==3)//Item type is 3 = shield
			{
				//Add to score
				Game.player.switchOnShield();
				Game.addInactiveItem(this.gameObject);
				
			}
			else
			{
				SFXManager.playSFX(1);//Play Electric Sound
				player = col.gameObject;//Assign player
				player.rigidbody2D.velocity = Vector2.zero;//Cancel out downwards force of the player
				jump = true; //Make jump var true 
				player.rigidbody2D.isKinematic = true; //disable gravity
				player.collider2D.enabled = false; //disable collider while using powerup jump

				Game.player.SwitchOnRocket();
				//this.mySpriteRenderer = this.Rocket;
				//myObjects[itemType].SetActive(true); //make the jump sprite active


				StartCoroutine("stopJumping"); //stop jump after certain seconds - depending on the type - edit freely
				mySpriteRenderer.enabled=false; 
			}
		}
		
	}

	IEnumerator stopJumping()
	{
		//Stop jumping - reset state
		yield return new WaitForSeconds(itemPower[itemType]);

		player.rigidbody2D.isKinematic = false;
		player.collider2D.enabled = true;

		//this.mySpriteRenderer = this.Stand;
		//myObjects[itemType].SetActive(false);
		Game.player.SwitchOffRocket ();

		jump = false;

		SFXManager.stopSFX();


	}



	void Update()
	{
		if(jump)
		{
			//Jump - move player object at a constact speed upwards
			player.transform.Translate(new Vector2(0,12*Time.deltaTime));
		}

		else if(Game.floor.transform.position.y > this.transform.position.y + 1)
		{
			Game.addInactiveItem(this.gameObject);
		}
	}
}