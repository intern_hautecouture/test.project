﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//using UnityEngine.Advertisements;
//using ChartboostSDK;

public class AdsManager : MonoBehaviour {

	public bool showAds;
	public bool showRevMob;
	public string RevMobID_IOS;
	public string RevMobID_ANDROID;
	public bool showUnityAds;
	public string UnityAdsGameID;
	//public bool showChartBoost;
	public string CharBoostID;




	public enum showAdsAfter{
		GAME,EVERY2GAMES,EVERY3GAMES,EVERY4GAMES,EVERY5GAMES,RANDOM,NEVER
	}



	List<string> adNetworks = new List<string>();
	void Start()
	{


		if(showRevMob)
			adNetworks.Add("RevMob");
		if(showUnityAds)
			adNetworks.Add("UnityAds");
		//if(showChartBoost)
			//adNetworks.Add("Chartboost");
	}


	public enum switchAds{
		YES,NO
	}

	public showAdsAfter showAdsAfterEvery;

	public switchAds switchBetweenAds;

	int counter;

	void showAd()
	{
		if(switchBetweenAds == switchAds.YES)
		{
			int i = Random.Range(0,adNetworks.Count);
			string adToShow = adNetworks[i];

			if(adToShow == "RevMob")
			{
				showRevMobAd();
			}
			else if(adToShow == "UnityAds")
			{
				showUnityAd();
			}
			else
			{
				//showChartboostAd();
			}
		}
	}

	/*void showChartboostAd()
	{
		Chartboost.cacheInterstitial(CBLocation.Default);
		Chartboost.showInterstitial(CBLocation.Default);
	}*/

	void showUnityAd()
	{
		Debug.Log("showing unityad");
		//Advertisement.Show();
	}

	void showRevMobAd()
	{
//		revmob.ShowFullscreen();
//		revmob.CreateFullscreen();
	}

	//------------- AWAKE



	//private RevMob revmob;
	
	void Awake() {
		if(showRevMob)
		{
			Dictionary<string,string> revmobids = new Dictionary<string,string>() {
				{"Android",RevMobID_ANDROID},
				{"IOS",RevMobID_IOS}
			};
//			revmob = RevMob.Start(revmobids, "AdsManager");


		}

		if(showUnityAds)
		{
			//Advertisement.Initialize(UnityAdsGameID,false);
		}

		/*if(showChartBoost)
		{

		}*/

	}





	//-------------

	public void showFullScreenAd()
	{
		if(showAds && showAdsAfterEvery != showAdsAfter.NEVER)
		{
			if(showAdsAfterEvery == showAdsAfter.GAME)
			{
				showAd();
			}
			else if(showAdsAfterEvery == showAdsAfter.EVERY2GAMES)
			{
				if(counter==1)
				{
					showAd();
					counter = 0;
				}
				else
					counter++;
			}
			else if(showAdsAfterEvery == showAdsAfter.EVERY3GAMES)
			{
				if(counter==2)
				{
					showAd();
					counter = 0;
				}
				else
					counter++;
			}
			else if(showAdsAfterEvery == showAdsAfter.EVERY4GAMES)
			{
				if(counter==3)
				{
					showAd();
					counter = 0;
				}
				else
					counter++;
			}
			else if(showAdsAfterEvery == showAdsAfter.EVERY5GAMES)
			{
				if(counter==4)
				{
					showAd();
					counter = 0;
				}
				else
					counter++;
			}
		}
	}
}
