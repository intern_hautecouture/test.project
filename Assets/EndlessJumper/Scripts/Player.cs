﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	public bool isMouseControl;

	public GameManager Game;

	public GameObject jumpBlue;
	public GameObject jumpOrange;
	public GameObject shieldBubble;
	public GameObject springBoots;
	public GameObject shield;

	public Sprite Stand;
	public Sprite Rocket; 
	public Sprite Bubble;
	public Sprite Wing;

	//public SpriteRenderer PlayerSprite;

	public SoundManager SFXManager;
	float leftBorder; 
	float rightBorder;

	int currentDirection;
	float timeDirection;

	Vector3 initScale;

	public bool isSpringBoot;

	SpriteRenderer thisRenderer;
	// Use this for initialization
	void Start () {


		thisRenderer = this.GetComponent<SpriteRenderer>();

		#if UNITY_IOS
			Application.targetFrameRate = 60;
		#endif

		Vector3 playerSize = renderer.bounds.size;
		// Here is the definition of the boundary in world point
		float distance = (transform.position - Camera.main.transform.position).z;
		leftBorder = Camera.main.ViewportToWorldPoint (new Vector3 (0, 0, distance)).x + (playerSize.x/2);
		rightBorder = Camera.main.ViewportToWorldPoint (new Vector3 (1, 0, distance)).x - (playerSize.x/2);
		timeDirection = Time.time;


		initScale = this.transform.localScale;
	}

	void OnMouseDown()
	{
		#if UNITY_EDITOR
		Screen.lockCursor =true;
		Screen.showCursor = false;
		#endif
	}


	public void switchOnShield()
	{
		//shieldBubble.SetActive(true);
		this.thisRenderer.sprite = this.Bubble;

	}

	public void switchOffShield()
	{
		//shieldBubble.SetActive(false);
		this.thisRenderer.sprite = this.Stand;
	}

	public void SwitchOnRocket()
	{
		this.thisRenderer.sprite = this.Rocket;
	}

	public void SwitchOffRocket()
	{
		this.thisRenderer.sprite = this.Stand;
	}


	public void switchOnSpringBoots()
	{
		//springBoots.SetActive(true);
		this.thisRenderer.sprite = this.Wing;

		this.GetComponent<BoxCollider2D>().center = new Vector2(0f,-0.5f);
		StartCoroutine("stopSpringBoots");
	}

	public void switchOffSpringBoots()
	{
		//springBoots.SetActive(false);
		this.thisRenderer.sprite = this.Stand;

		this.GetComponent<BoxCollider2D>().center = new Vector2(0f,-0.5f);
	}

	IEnumerator stopSpringBoots()
	{
		//Stop jumping - reset state
		yield return new WaitForSeconds(Game.springBootsTime);
		
		switchOffSpringBoots();
		isSpringBoot = false;
		
		
	}

	Vector3 mousePosition;
	// Update is called once per frame
	void Update () {

		if(!Game.isGameOver && !Game.isGamePaused)
		{
		if (Input.GetKeyDown("escape")) //If escape is pressed, mouse is released
		{
			Screen.lockCursor = false;
			Game.endGame();
			Screen.showCursor = true;
		}

			//Get mouse position
		mousePosition = Input.mousePosition;           
		mousePosition = Camera.main.ScreenToWorldPoint(mousePosition);
		mousePosition.y = 0f;
		#if UNITY_EDITOR || UNITY_WEBPLAYER
			Vector3 diff;
			if(isMouseControl){
				Debug.Log("is mouse control yes");
				//If Mouse control is being used, the player follows the mouse
				diff =  Vector3.MoveTowards(this.transform.localPosition, mousePosition,(0.1f * Time.time));}
			else
			{
				//Keyboard Control - use arrow keys to move the player
				Vector3 acc = Vector3.zero;
				if(Input.GetKey(KeyCode.LeftArrow))
				{
					acc.x = -0.1f;
					this.gameObject.transform.localScale = new Vector3(-initScale.x,initScale.y,initScale.z);
				}
				if(Input.GetKey(KeyCode.RightArrow))
				{
					acc.x = 0.1f;
					this.gameObject.transform.localScale = new Vector3(initScale.x,initScale.y,initScale.z);
				}



				diff = Vector3.MoveTowards(this.transform.localPosition,this.transform.localPosition + acc,(0.5f * Time.time));

			}
		#else

			//Accelerometer Control - default for mobile builds

			Vector3 acc = Input.acceleration;
			acc.y = 0f;
			acc.z = 0f;
			Vector3 diff =  Vector3.MoveTowards(this.transform.localPosition,this.transform.localPosition + (acc*0.7f),(0.5f * Time.time));

			if(acc.x < - 0.07f)
			{
				this.gameObject.transform.localScale = new Vector3(-1,1,1);
			}
			if(acc.x > 0.07f)
			{
				this.gameObject.transform.localScale = new Vector3(1,1,1);
			}


		#endif

		diff.y = this.transform.localPosition.y;
		diff.z = 0f;

		if(diff.x<leftBorder-1f)
		{
			diff.x = rightBorder;
		}
		else if(diff.x>rightBorder+1f)
		{
			diff.x = leftBorder;
		}

		this.transform.localPosition = diff;
		}

	}

	public void switchOn(string name)
	{
		if(name.Contains("j2")) //Switch on the jump sprite behind the player
			//jumpBlue.SetActive(true);
			this.thisRenderer.sprite = this.Rocket;
		else
			//jumpOrange.SetActive(true);
			this.thisRenderer.sprite = this.Rocket;
	}
	public void switchOff(string name)
	{
		if(name.Contains("j2"))//Switch on the jump sprite behind the player
			//jumpBlue.SetActive(false);
			this.thisRenderer.sprite = this.Stand;
		else
			//jumpOrange.SetActive(false);
			this.thisRenderer.sprite = this.Stand;
	}

	public void jump(float x)
	{

		if(isSpringBoot)
		{
			x*=1.7f;
			SFXManager.playSFX(4);//Play Jump Sound with spring
		}
		else
		{
			SFXManager.playSFX(0);//Play Jump Sound Normal
		}

		//Jump (12*x) force - change force for lower jump or change tile jump in the tile.cs script
		this.gameObject.rigidbody2D.velocity = Vector2.zero;
		this.gameObject.rigidbody2D.AddForce(new Vector2(0f,12f*x),ForceMode2D.Impulse);


	}



	void OnTriggerEnter2D(Collider2D col)
	{

		if(col.name.Contains("platform"))
		{
			//Jump if the object is platform
			Game.player.jump(1);
		}

		else if(this.gameObject.rigidbody2D.velocity.y <= 0 )
		{
			if(col.gameObject.name.Contains("Floor"))
			{
				//If the player hits the floor object, then it means he has fallen - end game
				Game.endGame();
			}
		}
	}
}
