﻿using UnityEngine;
using System.Collections;

public class Stretch2DSpriteBackground : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
		transform.localScale = new Vector3(1,1,1);
		
		float width = this.GetComponent<SpriteRenderer>().sprite.bounds.size.x;
		float height = this.GetComponent<SpriteRenderer>().sprite.bounds.size.y;
		
		float worldScreenHeight = Camera.main.orthographicSize * 2.0f;
		float worldScreenWidth = worldScreenHeight / Screen.height * Screen.width;


		transform.localScale = new Vector3(worldScreenWidth / width,worldScreenHeight / height,1);

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
