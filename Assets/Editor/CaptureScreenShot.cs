﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class CaptureScreenShot : Editor {

    [MenuItem("HC/CaptureGViewScreenShot #F1")]
    static void Capture()
    {
        string name = System.IO.Directory.GetCurrentDirectory() + System.DateTime.Now.ToString("MMdd-HHmm-ss") + ".png";

        Application.CaptureScreenshot(name);

        System.Type T = typeof(UnityEditor.EditorWindow).Assembly.GetType("UnityEditor.GameView");
        EditorWindow gameView = EditorWindow.GetWindow(T);
        gameView.Repaint();
    }
}
