﻿using UnityEngine;
using System.Collections;

public class GUIManager : MonoBehaviour {

	public GameManager Game;

	//Panels to Use
	public GameObject MainScreenPanel;
	public GameObject EndScreenPanel;
	public GameObject PauseScreenPanel;
	public GameObject IngameScreenPanel;
	public GameObject SettingsPanel;
	public GameObject InstructionsPanel;

	//Buttons to use in GUI
	public UIButton soundBtn;
	public GameObject gamecenterBtn;

	//Variables Static
	public static bool isSoundOn = true;
	public static bool isRestart;

	// Use this for initialization
	void Start () {
	
		//PlayerPrefs.DeleteAll();
		#if UNITY_IPHONE
			gamecenterBtn.SetActive(true);
		#endif

	

		if(GUIManager.isRestart)
		{
			playButtonClicked();
		}
	}

	// ----------- PANEL TRANSITIONS -------------- //
	

	public void showMainPanel()
	{
		Time.timeScale = 1;
		IngameScreenPanel.SetActive(false);
		PauseScreenPanel.SetActive(false);
		MainScreenPanel.SetActive(true);
	}


	public void showPausePanel()
	{
		Time.timeScale = 0;
		PauseScreenPanel.SetActive(true);
	}

	public void showIngamePanel()
	{
		Time.timeScale = 1;
		PauseScreenPanel.SetActive(false);
		MainScreenPanel.SetActive(false);
		IngameScreenPanel.SetActive(true);
		InstructionsPanel.SetActive(false);
	}


	public void showGameOverPanel()
	{
		Time.timeScale = 0;
		IngameScreenPanel.SetActive(false);
		EndScreenPanel.SetActive(true);
	}

	public void showInstructionsPanel()
	{
		IngameScreenPanel.SetActive(false);
		PauseScreenPanel.SetActive(false);
		MainScreenPanel.SetActive(true);
		InstructionsPanel.SetActive(true);
	}





	// ----------- / END PANEL TRANSITIONS / -------------- //



	// ----------- MAIN SCREEN FUNCTIONS -------------- //


	public void playButtonClicked()
	{

		if(PlayerPrefs.HasKey("hasSeenInstructions")) //If it exists
		{
			if(PlayerPrefs.GetInt("hasSeenInstructions") ==0) //Is it 0, then show instructions for first run
			{
				showInstructionsPanel();
				PlayerPrefs.SetInt("hasSeenInstructions",1);
			}
			else //not 0 then start game
			{
				//Kamcord.StartRecording ();
				Game.resumeGame(); //START GAME 
				showIngamePanel();
			}
		}
		else //does not exist, show instructions for first run
		{
			showInstructionsPanel();
			PlayerPrefs.SetInt("hasSeenInstructions",1);
		}
		

	
	}

	public void settingsButtonClicked()
	{
		//Show/Hide Settings Panel
		if(SettingsPanel.activeSelf)
			SettingsPanel.SetActive(false);
		else
			SettingsPanel.SetActive(true);
	}

	public void infoButtonClicked()
	{
		//string url = GameObject.Find("GameSettings").GetComponent<GameDetails>().websiteUrl;
//		Application.OpenURL(url);
	}

	public void likeButtonClicked()
	{
		//string url = GameObject.Find("GameSettings").GetComponent<GameDetails>().facebookPageUrl;
	//	Application.OpenURL(url);
	}

	public void gamecenterButtonClicked()
	{
		#if UNITY_IPHONE
			Social.ShowLeaderboardUI();
		#endif
	}

	public void soundButtonClicked()
	{
		if(GUIManager.isSoundOn)
		{
			GUIManager.isSoundOn = false;
			soundBtn.normalSprite = "volumeOffButton";
			AudioListener.pause = true;
		}
		else
		{
			GUIManager.isSoundOn = true;
			soundBtn.normalSprite = "volumeOnButton";
			AudioListener.pause = false;
		}
	}


	// ----------- END MAIN SCREEN FUNCTIONS -------------- //


	// ----------- GAMEOVER SCREEN FUNCTIONS -------------- //

	public void quitButtonClicked()
	{
		GUIManager.isRestart = false;
		Application.LoadLevel("Main"); //Reload Level
	}

	public void videoButtonClicked()
	{
		//Kamcord.ShowView ();
	}

	public void gameCenterButtonClicked()
	{
		Social.ShowLeaderboardUI();
	}

	public void restartButtonClicked()
	{
		GUIManager.isRestart = true;
		Application.LoadLevel("Main"); //Reload Level
	}
	

	// ----------- END GAMEOVER SCREEN FUNCTIONS -------------- //

	// ----------- INGAME SCREEN FUNCTIONS -------------- //
	
	public void pauseButtonClicked()
	{
		Time.timeScale = 0;
		showPausePanel();
	}
	
	public void resumeButtonClicked()
	{
		Time.timeScale = 1;
		showIngamePanel();
	}
	
	public void quitButtonClickedIngame()
	{
		GUIManager.isRestart = false;
		Time.timeScale = 1;
		Application.LoadLevel("Main"); //Restart Scene
	}

	// ----------- END INGAME SCREEN FUNCTIONS -------------- //

}
